package com.glu.app;

import android.widget.AutoCompleteTextView;
import android.widget.EditText;

public class MealPartsRowView {
	private AutoCompleteTextView productNameTextView;
	private EditText foodQuantityEdittext;
	private int index;
	
	public MealPartsRowView(AutoCompleteTextView autoComplete, EditText editText, int index) {
		this.productNameTextView = autoComplete;
		this.foodQuantityEdittext = editText;
		this.index = index;
	}
}

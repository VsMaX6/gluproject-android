package com.glu.app;

import java.util.ArrayList;
import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

public class Meal implements Parcelable {

	public Meal()
	{
		mealName = "";
		mealParts = new ArrayList<MealPart>();
		mealTime = new Date();
		insulinDose = 0.0f;
	}
	public String mealName;
	public ArrayList<MealPart> mealParts;
	public Date mealTime;
	public float insulinDose;
	
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mealName);
		dest.writeLong(mealTime.getTime());
		dest.writeTypedList(mealParts);
		dest.writeFloat(insulinDose);
	}
	
	public float getInsulinDose(float IWRatio) {
		float sum = 0.0f;
		for(MealPart mealPart: mealParts) {
			sum += mealPart.carbohydratesPer100g * mealPart.foodQuantity;
		}
		return sum * IWRatio;
	}
	
	public int describeContents() {
        return 0;
    }
	
	public static final Parcelable.Creator<Meal> CREATOR = new Parcelable.Creator<Meal>() {
        public Meal createFromParcel(Parcel in) {
            return new Meal(in);
        }

        public Meal[] newArray(int size) {
            return new Meal[size];
        }
    };
    
    private Meal(Parcel in) {
    	this.mealName = in.readString();
    	this.mealTime = new Date(in.readLong());
    	mealParts = new ArrayList<MealPart>();
    	in.readTypedList(mealParts, MealPart.CREATOR);
    	this.insulinDose = in.readFloat();
    }

	public String getMealPartsTextForView() {
		String text = mealParts.size() > 0 ? mealParts.get(0).productName : "";
		for(int i = 1; i < mealParts.size(); i++) {
			text += ", " + mealParts.get(i).productName;
		}
		return text;
	}
}

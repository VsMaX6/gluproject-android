package com.glu.app;

import android.os.Parcel;
import android.os.Parcelable;

public class MealPart implements Parcelable {
	public long mealPartId;
	public long productId;
	public String productName;
	public float foodQuantity;
	public float carbohydratesPer100g;
	
	public MealPart()
	{
		productId = 0;
		productName = "";
		foodQuantity = 0.0f;
		carbohydratesPer100g = 0.0f;
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
    public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(mealPartId);
        dest.writeLong(productId);
        dest.writeString(productName);
        dest.writeFloat(foodQuantity);
    }
	
	private MealPart(Parcel in) {
		mealPartId = in.readLong();
		productId = in.readLong();
		productName = in.readString();
		foodQuantity = in.readFloat();
	}
	
	public static final Parcelable.Creator<MealPart> CREATOR = 
			new Parcelable.Creator<MealPart>() {
		public MealPart createFromParcel(Parcel in) {
            return new MealPart(in);
        }

        public MealPart[] newArray(int size) {
            return new MealPart[size];
        }
	};
}

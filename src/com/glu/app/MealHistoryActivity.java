package com.glu.app;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MealHistoryActivity extends Activity {

	ListView mealsListView;
	CustomMealAdapter mAdapter;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meal_list_activity);
        mealsListView = (ListView) findViewById(R.id.mealHistory);
        TextView emptyText = (TextView)findViewById(android.R.id.empty);
        mealsListView.setEmptyView(emptyText);
        mAdapter = new CustomMealAdapter(this);
        mealsListView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
	public void onAddMealButtonClick(View view){
		Intent addMealIntent = new Intent(this, AddMealActivity.class);
		addMealIntent.putExtra("requestCode", Utilities.ADD_MEAL_CODE);
		startActivityForResult(addMealIntent, Utilities.ADD_MEAL_CODE);
    }
    
    public void onRemoveMealButtonClick(View view) {
    	int toRemovePosition = mealsListView.getCheckedItemPosition();
    	if(toRemovePosition >= 0)
    		removeItemFromList(toRemovePosition);
    	mealsListView.setItemChecked(toRemovePosition, false);
    }
    
    public void onAddProductButtonClick(View view) {
    	Intent addProductIntent  = new Intent(this, AddProductActivity.class);
    	addProductIntent.putExtra("requestCode", Utilities.ADD_PRODUCT_CODE);
		startActivityForResult(addProductIntent, Utilities.ADD_PRODUCT_CODE);
    }
    
    private void removeItemFromList(int toRemovePosition) {
		Meal itemToRemove = (Meal) mAdapter.getItem(toRemovePosition);
		mAdapter.remove(itemToRemove);
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent dataIntent) {
		Bundle extras = dataIntent.getExtras();
		int extraRequestCode = extras.getInt("requestCode");
		if (resultCode == RESULT_OK && extraRequestCode == Utilities.ADD_MEAL_CODE) {
			Meal meal = (Meal) dataIntent.getExtras().get("Meal");
			mAdapter.add(meal);
		}
		if (resultCode == RESULT_OK && extraRequestCode == Utilities.ADD_FAVOURITE_MEAL_CODE) {
			Toast toast = Toast.makeText(this,  "Dodano nowy ulubiony posiłek", Toast.LENGTH_LONG);
			toast.show();
		}
		if (resultCode == RESULT_OK && extraRequestCode == Utilities.ADD_PRODUCT_CODE) {
			Toast toast = Toast.makeText(this,  "Dodano nowy produkt", Toast.LENGTH_LONG);
			toast.show();
		}
	}
	
	private static class CustomMealAdapter extends BaseAdapter {
		
		private ArrayList<Meal> _meals;
		private Context _context;
		private LayoutInflater mInflater;
		
		static class ViewHolder {
			TextView mealNameTextView;
			TextView mealTimeTextView;
			TextView insulinDoseTextView;
			TextView mealPartsTextView;
		}
		
		public CustomMealAdapter(Context context) {
			_meals = new ArrayList<Meal>();
			_context = context;
			mInflater = LayoutInflater.from(context);
		}
		
		@Override
		public int getCount() {
			return _meals.size();
		}
		
		@Override
		public long getItemId(int position) {
			return position;
		}
		
		@Override
		public Object getItem(int position) {
			if(position >= 0)
				return _meals.get(position);
			return null;
		}
		
		public void add(Meal meal) {
			_meals.add(meal);
			notifyDataSetChanged();
		}
		
		public void remove(Meal meal) {
			if(meal == null)
				return;
			_meals.remove(meal);
			notifyDataSetChanged();
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if(convertView == null) {
				convertView = mInflater.inflate(R.layout.meal_listview_row, null);
				holder = new ViewHolder();
				holder.mealNameTextView = (TextView) convertView.findViewById(R.id.mealNameTextView);
				holder.mealTimeTextView = (TextView) convertView.findViewById(R.id.mealTimeTextView);
				holder.insulinDoseTextView = (TextView) convertView.findViewById(R.id.insulinDoseTextView);
				holder.mealPartsTextView = (TextView) convertView.findViewById(R.id.mealPartsTextView);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			Meal meal = (Meal) getItem(position);
			holder.mealNameTextView.setText(meal.mealName);
			
			SimpleDateFormat mealDateFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
			holder.mealTimeTextView.setText(mealDateFormat.format(meal.mealTime));
			holder.insulinDoseTextView.setText(Utilities.trimDecimalsToString(meal.insulinDose));
			
			holder.mealPartsTextView.setText(meal.getMealPartsTextForView());
			
			return convertView;
		}
	}
}

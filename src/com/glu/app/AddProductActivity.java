package com.glu.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AddProductActivity extends Activity {
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product_activity);
    }
	
	public void onOkButtonClick(View view) {
		Intent intent = getIntent();
		intent.putExtra("requestCode", Utilities.ADD_PRODUCT_CODE);
		setResult(Activity.RESULT_OK, intent);
		finish();
	}
}

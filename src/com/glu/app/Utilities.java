package com.glu.app;

import java.text.DecimalFormat;

import android.widget.TextView;

public class Utilities {
	public static boolean isTooLarge (TextView text, String newText) {
	    float textWidth = text.getPaint().measureText(newText);
	    return (textWidth >= text.getMeasuredWidth ());
	}
	
	public static String trimDecimalsToString(float foodQuantity) {
		DecimalFormat insulinDoseFormat = new DecimalFormat("#");
		return insulinDoseFormat.format(foodQuantity);
	}
	
	public static final int ADD_MEAL_CODE = 1001;
	public static final int ADD_FAVOURITE_MEAL_CODE = 1002;
	public static final int ADD_PRODUCT_CODE = 1003;
}
